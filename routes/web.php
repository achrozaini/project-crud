<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PesanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('info', function () {
    return view('info');
});
Route::get('pesan',[PesanController::class,'index'])->name('pesan');
Route::get('tambahpesan',[PesanController::class,'create'])->name('tambahpesan');
Route::put('simpan',[PesanController::class,'store'])->name('simpan');
Route::get('/edit/{id}', [PesanController::class,'edit'])->name('edit');
Route::put('/update/{id}', [PesanController::class,'update'])->name('update');
Route::get('/delete/{id}', [PesanController::class,'destroy'])->name('delete');