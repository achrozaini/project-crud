<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pesawat extends Model
{
    protected $table = "pesawats";
    protected $primaryKey = "id";
    protected $fillable =[
        'id','nama','alamat','nama_pesawat','tujuan_pesawat'
    ];
}
