<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pesawat;

class PesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtpesan = pesawat::all();
        return view('pemesanan', compact('dtpesan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahpesan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        pesawat::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'nama_pesawat' => $request->nama_pesawat,
            'tujuan_pesawat' => $request->tujuan_pesawat,
        ]);
        return redirect('pesan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = pesawat::find($id);
        return view('edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $data = pesawat::findorfail($id);
        $save= $data->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'nama_pesawat' => $request->nama_pesawat,
            'tujuan_pesawat' => $request->tujuan_pesawat,
        ]);
        if($save){
            $list = pesawat::all();
            return redirect()->route('pesan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list = pesawat::findorfail($id);
        $list->delete();
        return back();
    }
}
