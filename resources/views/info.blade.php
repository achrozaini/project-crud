<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Pemesanan Tiket </title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Tiket Pesawat</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="info">Info</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('pesan')}}">Pemesanan</a>
      </li>
    </ul>
  </div>
</nav>
    <h1><marquee>INFO KAMI </marquee></h1>
    <div class="card-body">
    <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">NAMA KOTA</th>
      <th scope="col">HARGA PESAWAT</th>
     
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>madura-surabaya</td>
      <td>35000</td>
      
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>madura-malang</td>
      <td>50000</td>
      
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>madura-sidoarjo</td>
      <td>40000</td>
      
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>madura-jakarta</td>
      <td>600000</td>
    </tr>
    <tr>
      <th scope="row">5</th>
      <td>madura-bali</td>
      <td>900000</td>
    </tr>
    <tr>
      <th scope="row">6</th>
      <td>madura-bandung</td>
      <td>600000</td>
    </tr>
    <tr>
      <th scope="row">7</th>
      <td>surabaya-malang</td>
      <td>30000</td>
    </tr>
    <tr>
      <th scope="row">8</th>
      <td>srabaya-jakarta</td>
      <td>500000</td>
    </tr>
    <tr>
      <th scope="row">9</th>
      <td>surabaya bali</td>
      <td>750000</td>
    </tr>
    <tr>
      <th scope="row">10</th>
      <td>surabaya-kalimantan</td>
      <td>650000</td>
    </tr>
    <tr>
      <th scope="row">11</th>
      <td>madura-kalimantan</td>
      <td>700000</td>
    </tr>
    <tr>
      <th scope="row">12</th>
      <td>madura-papua</td>
      <td>1100000</td>
    </tr>
    <tr>
      <th scope="row">13</th>
      <td>surabaya-papua</td>
      <td>1000000</td>
    </tr>
  </tbody>
</table>
   
  
     
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>